const electron = require('electron');
const {
  app,
  BrowserWindow,
  ipcMain,
  dialog
} = electron;

const fs = require('fs');
const path = require('path');
const Jsonfile = require('jsonfile');

const mime = require('mime-types')

const imagemin = require('imagemin')
const imageminPngquant = require('imagemin-pngquant');
const imageminsvgo = require('imagemin-svgo');
const imagemingifsicle = require('imagemin-gifsicle');
const imageminmozjpeg = require('imagemin-mozjpeg');

const ENTRIES_JSON_FILE_PATH = app.getPath('userData') + '/entries.json';
const SETTINGS_JSON_FILE_PATH = app.getPath('userData') + '/settings.json';
let entries = [];
let settings = null;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 480,
    height: 400,
    minWidth: 480,
    minHeight: 400,
    title: 'Minimage',
    titleBarStyle: 'hidden',
    webPreferences: {
      experimentalFeatures: true
    },
  });

  // and load the index.html of the app.
  win.loadURL('http://localhost:3000');

  // Open the DevTools.
  win.webContents.openDevTools({
    mode: 'detach'
  });

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

ipcMain.on('drop-files', (event, files) => {
  optimizeFiles(files);
});

ipcMain.on('clear-file-list', () => {
  entries = [];
  writeEntries([]);
});

ipcMain.on('browse-files', () => {
  dialog.showOpenDialog({
    properties: ['openFile', 'openDirectory', 'multiSelections']
  }, function (result) {
    const files = changeImagesPathsToFiles(result);
    optimizeFiles(files);
  });
});

ipcMain.on('get-settings', (event) => {
  const settings = getSettings();
  event.sender.send('get-settings', settings);
});

ipcMain.on('save-settings', (event, settings) => {
  writeSettings(settings);
});

function optimizeFiles(files) {
  files = changeDirectriesToFiles(files);
  writeEntries(files);

  const settings = getSettings();
  files.forEach((file) => {
    imagemin([file.path], `${path.dirname(file.path)}/`, {
      plugins: [
        imageminmozjpeg(settings.jpg),
        imageminsvgo(settings.svg),
        imagemingifsicle(settings.gif),
        imageminPngquant(settings.png)
      ]
    }).then((result) => {
      const stats = getFileStats(file.path);
      file.status = 'ok';
      file.result = stats.size;
      file.savedBytesInPercent = ((file.size - file.result) * 100) / file.size;
      writeEntries(files);
    }).catch((error) => {
      file.status = 'error';
      console.log(error);
      writeEntries(files);
    })
  });
}

function writeEntries(files) {
  files.forEach((file) => {
    let entry = entries.find((entry) => {
      return entry.path == file.path
    });
    if (entry) {
      entry = file;
    } else {
      entries.push(file);
    }
  });
  Jsonfile.writeFile(ENTRIES_JSON_FILE_PATH, entries, function () {});

  if (win) {
    win.webContents.send('update-file-list', entries);
  }
}

function changeImagesPathsToFiles(paths) {
  const files = [];
  paths.forEach((item) => {
    const type = mime.lookup(item);
    if (type.toString().includes('image')) {
      files.push(createFileFromPath(item));
    } else if (fs.lstatSync(item).isDirectory()) {
      files.push({
        path: item
      });
    }
  });
  return files;
}

function changeDirectriesToFiles(files) {
  files.forEach((file, index) => {
    if (fs.lstatSync(file.path).isDirectory()) {
      let imagesInPath = getAllImagesPathInDir(file.path);

      imagesInPath.forEach((image) => {
        files.push(createFileFromPath(image));
      });

      files.concat(imagesInPath);
      files.splice(index, 1);
    }
  });
  return files;
}

function getAllImagesPathInDir(dir, filelist = []) {
  fs.readdirSync(dir).forEach(file => {
    const dirFile = path.join(dir, file);
    try {
      filelist = getAllImagesPathInDir(dirFile, filelist);
    } catch (err) {
      if (err.code === 'ENOTDIR' || err.code === 'EBUSY') {
        const type = mime.lookup(dirFile);
        if (type.toString().includes('image')) {
          filelist = [...filelist, dirFile];
        }
      } else throw err;
    }
  });
  return filelist;
}

function getFileStats(path) {
  return fs.statSync(path);
}

function createFileFromPath(pathString) {
  const stats = getFileStats(pathString);
  return {
    name: path.basename(pathString),
    path: pathString,
    size: stats.size,
    type: mime.lookup(pathString),
    status: 'process'
  };
}

function getSettings() {
  if (fs.existsSync(SETTINGS_JSON_FILE_PATH)) {
    settings = Jsonfile.readFileSync(SETTINGS_JSON_FILE_PATH);
  } else {
    settings = {
      jpg: {
        quality: 100,
        progressive: true, // Lossless conversion to progressive.
      },
      png: {
        quality: 100,
        floyd: 0.5, // Controls level of dithering (0 = none, 1 = full).
      },
      gif: {
        optimizationLevel: 1, // Optimization level between 1 and 3
        colors: 256 // Reduce the number of distinct colors. Num must be between 2 and 256.
      },
      svg: {
        cleanupAttrs: true, // Cleanup attributes from newlines, trailing, and repeating spaces
        minifyStyles: true, // Minify <style> elements
        convertColors: true, // Convert colors (from rgb() to #rrggbb, from #rrggbb to #rgb)
      }
    }
  }
  return settings;
}

function writeSettings(newSettings = {}) {
  Jsonfile.writeFile(SETTINGS_JSON_FILE_PATH, Object.assign({}, settings, newSettings), function () {});
}