import React, { Component } from 'react';
import { ipcRenderer } from 'electron';
import './Settings.css';

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      settings: null
    };

    ipcRenderer.on('get-settings', (event, newSettings) => {
      this.updateSettings(newSettings);
    });

    ipcRenderer.send('get-settings');
  }

  componentWillUnmount = () => {
    ipcRenderer.removeAllListeners('get-settings');
  }

  updateSettings = (newSettings) => {
    this.setState({
      settings: newSettings
    });
  }

  updateTypeSettings = (newSettings, type) => {
    const settings = this.state.settings;
    settings[type] = Object.assign({}, this.state.settings[type], newSettings);
    this.updateSettings(settings);
  }

  closeSettings = () => {
    this.props.onCloseSettings();
  }

  saveSettings = () => {
    ipcRenderer.send('save-settings', this.state.settings);
    this.closeSettings();
  }

  render() {
    return (
      <div className="settings">
        <div className="drag-zone"></div>
        {this.state.settings ?
          <div className="settings-box">
            <p className="settings-title">JPG</p>
            <div className="settings-item">
              <label className="form-switch">
                Convert to progressive
              <input type="checkbox" checked={this.state.settings.jpg.progressive}
                  onChange={() => this.updateTypeSettings({ progressive: !this.state.settings.jpg.progressive }, 'jpg')} />
                <i></i>
              </label>
            </div>
            <div className="settings-item">
              <div className="form-slider">
                Quality
              <label>
                  {this.state.settings.jpg.quality}
                  <input type="range" min="1" max="100" step="1" className="slider"
                    value={this.state.settings.jpg.quality}
                    onChange={(e) => this.updateTypeSettings({ quality: ~~e.target.value }, 'jpg')} //~~ parse to int
                  />
                </label>
              </div>
            </div>

            <p className="settings-title">PNG</p>
            <div className="settings-item">
              <div className="form-slider">
                Quality
              <label>
                  {this.state.settings.png.quality}
                  <input type="range" min="1" max="100" step="1" className="slider"
                    value={this.state.settings.png.quality}
                    onChange={(e) => this.updateTypeSettings({ quality: ~~e.target.value }, 'png')} //~~ parse to int
                  />
                </label>
              </div>
            </div>
            <div className="settings-item">
              <div className="form-slider">
                Floyd
              <label>
                  {this.state.settings.png.floyd}
                  <input type="range" min="0.1" max="1" step="0.1" className="slider"
                    value={this.state.settings.png.floyd}
                    onChange={(e) => this.updateTypeSettings({ floyd: parseFloat(e.target.value) }, 'png')}
                  />
                </label>
              </div>
            </div>

            <p className="settings-title">GIF</p>
            <div className="settings-item">
              <p className="settings-item-title">Colors</p>
              <ul className="radio-list">
                <li className="radio">
                  <input type="radio" name="colors"
                    value={256}
                    checked={this.state.settings.gif.colors === 256}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ colors: 256 }, 'gif')}
                    className="radio-style">
                  </label>
                  256
              </li>
                <li className="radio">
                  <input type="radio" name="colors"
                    value={128}
                    checked={this.state.settings.gif.colors === 128}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ colors: 128 }, 'gif')}
                    className="radio-style">
                  </label>
                  128
              </li>
                <li className="radio">
                  <input type="radio" name="colors"
                    value={64}
                    checked={this.state.settings.gif.colors === 64}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ colors: 64 }, 'gif')}
                    className="radio-style">
                  </label>
                  64
              </li>
                <li className="radio">
                  <input type="radio" name="colors"
                    value={32}
                    checked={this.state.settings.gif.colors === 32}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ colors: 32 }, 'gif')}
                    className="radio-style">
                  </label>
                  32
              </li>
                <li className="radio">
                  <input type="radio" name="colors"
                    value={16}
                    checked={this.state.settings.gif.colors === 16}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ colors: 16 }, 'gif')}
                    className="radio-style">
                  </label>
                  16
              </li>
              </ul>
            </div>

            <div className="settings-item">
              <p className="settings-item-title">Optimization Level</p>
              <ul className="radio-list">
                <li className="radio">
                  <input type="radio" name="optimizationLevel"
                    value={1}
                    checked={this.state.settings.gif.optimizationLevel === 1}
                    onChange={() => {}}                    
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ optimizationLevel: 1 }, 'gif')}
                    className="radio-style" htmlFor="1">
                  </label>
                  1
              </li>
                <li className="radio">
                  <input type="radio" name="optimizationLevel"
                    value={2}
                    checked={this.state.settings.gif.optimizationLevel === 2}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ optimizationLevel: 2 }, 'gif')}
                    className="radio-style" htmlFor="2">
                  </label>
                  2
              </li>
                <li className="radio">
                  <input type="radio" name="optimizationLevel"
                    value={3}
                    checked={this.state.settings.gif.optimizationLevel === 3}
                    onChange={() => {}}
                  />
                  <label
                    onClick={() => this.updateTypeSettings({ optimizationLevel: 3 }, 'gif')}
                    className="radio-style" htmlFor="3">
                  </label>
                  3
              </li>
              </ul>
            </div>

            <p className="settings-title">SVG</p>
            <div className="settings-item">
              <label className="form-switch">
                Clean Up Attributes
              <input type="checkbox" checked={this.state.settings.svg.cleanupAttrs}
                  onChange={() => this.updateTypeSettings({ cleanupAttrs: !this.state.settings.svg.cleanupAttrs }, 'svg')} />
                <i></i>
              </label>
            </div>
            <div className="settings-item">
              <label className="form-switch">
                Convert Colors
              <input type="checkbox" checked={this.state.settings.svg.convertColors}
                  onChange={() => this.updateTypeSettings({ convertColors: !this.state.settings.svg.convertColors }, 'svg')} />
                <i></i>
              </label>
            </div>
            <div className="settings-item">
              <label className="form-switch">
                Minify Styles
              <input type="checkbox" checked={this.state.settings.svg.minifyStyles}
                  onChange={() => this.updateTypeSettings({ minifyStyles: !this.state.settings.svg.minifyStyles }, 'svg')} />
                <i></i>
              </label>
            </div>
            <div className="controls">
              <a onClick={this.closeSettings} className="btn btn-danger">Cancel</a>
              <a onClick={this.saveSettings} className="btn btn-success">Save</a>
            </div>
          </div>
          : null}
      </div>
    );
  }
}

export default Settings;
