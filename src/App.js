import React from 'react';
import Navbar from './components/Navbar/Navbar';
import Settings from './components/Settings/Settings';
import FileDrop from 'react-file-drop';
import { ipcRenderer } from 'electron'

import './App.css';

import pngIcon from './assets/icons/png3.svg';
import jpgIcon from './assets/icons/jpg3.svg';
import svgIcon from './assets/icons/svg3.svg';
import gifIcon from './assets/icons/gif3.svg';
import undefinedIcon from './assets/icons/undefined-file.svg';

import cancelIcon from './assets/icons/cancel.svg';
import okIcon from './assets/icons/ok.svg';
import errorIcon from './assets/icons/error.svg';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fileList: [],
      isSettingsOpened: false
    };

    ipcRenderer.on('update-file-list', (event, fileList) => {
      this.setFileList(fileList);
    });
  }

  componentWillUnmount = () => {
    ipcRenderer.removeAllListeners('update-file-list');
  }

  handleDrop = (droppedFileList, event) => {
    //prevent opening file in browser
    event.preventDefault();

    // Convert FileList to Array
    const fileList = [...droppedFileList];

    // Convert all files to objects
    const files = [];
    fileList.forEach((file) => {
      if (file.type.includes('image') || file.type === '') {
        files.push({
          name: file.name,
          path: file.path,
          size: file.size,
          type: file.type,
          status: 'process'
        });
      }
    });

    ipcRenderer.send('drop-files', files);
  }

  setFileList = (files) => {
    this.setState({
      fileList: files
    });
  }

  getFileIcon = (file) => {
    const type = file.type.toLowerCase();
    if (type.includes('png')) {
      return pngIcon;
    } else if (type.includes('jpg') || type.includes('jpeg')) {
      return jpgIcon;
    } else if (type.includes('gif')) {
      return gifIcon;
    } else if (type.includes('svg')) {
      return svgIcon;
    } else {
      return undefinedIcon;
    }
  }

  getStatusIcon = (file) => {
    const status = file.status.toLowerCase();
    if (status === 'process') {
      return cancelIcon;
    } else if (status === 'ok') {
      return okIcon;
    } else {
      return errorIcon;
    }
  }

  browseFiles = () => {
    ipcRenderer.send('browse-files');
  }

  toggleSettings = () => {
    this.setState({ isSettingsOpened: !this.state.isSettingsOpened });
  }

  clearFileList = () => {
    this.setFileList([]);
    ipcRenderer.send('clear-file-list');
  }

  render() {
    return (
      <div className="App">
        <Navbar onOpenSettings={this.toggleSettings} onRefreshFiles={this.clearFileList} />
        <div className="App-main-container">
          <FileDrop onDrop={this.handleDrop}>
            {this.state.fileList.length ?
              <div className="main-files">
                <ul className="file-list">
                  {
                    this.state.fileList.map(file =>
                      <li key={file.path}>
                        <div className="file-list-item">
                          <img src={this.getFileIcon(file)} className="file-icon" alt={file.type} />
                          <div className="file-list-item-info">
                            <p className="file-title">{file.name}</p>
                            <p className="file-subtitle">Size: {file.result ? file.result : file.size}
                              {file.savedBytesInPercent ? <span> | Saved: {Math.ceil(file.savedBytesInPercent)}%</span> : null}
                            </p>
                            <span className="file-status">
                              <img src={this.getStatusIcon(file)} className="status-icon" alt={file.status} />
                            </span>
                          </div>
                        </div>
                      </li>
                    )
                  }
                </ul>
              </div>
              :
              <div className="main-empty">
                <div className="files-images">
                  <img src={jpgIcon} className="files-icon files-icon-first" alt="jpg" />
                  <img src={pngIcon} className="files-icon files-icon-center" alt="png" />
                  <img src={svgIcon} className="files-icon files-icon-last" alt="gif" />
                </div>
                <h2>Drag & Drop</h2>
                <p>images here, or <a onClick={this.browseFiles}>browse</a></p>
              </div>
            }
          </FileDrop>
        </div>
        {this.state.isSettingsOpened ? <Settings onCloseSettings={this.toggleSettings} /> : null}
      </div>
    );
  }
}

export default App;
